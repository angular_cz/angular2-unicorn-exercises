import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Room } from './model/chat';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html'
})
export class ChatComponent implements OnInit {

  rooms: Room[];

  constructor(route: ActivatedRoute) {
    this.rooms = route.snapshot.data.rooms;

  }

  ngOnInit() {
  }

}
