import { Component, forwardRef } from '@angular/core';
import {
    AbstractControl,
    AsyncValidatorFn,
    ControlValueAccessor,
    FormBuilder,
    FormGroup,
    NG_VALIDATORS,
    NG_VALUE_ACCESSOR,
    Validator,
    Validators
} from '@angular/forms';
import { interval, of, timer } from 'rxjs';
import { first, map } from 'rxjs/operators';

@Component({
    selector: 'app-user-contact-form',
    templateUrl: './user-contact-form.component.html',
    styleUrls: ['./user-contact-form.component.css'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => UserContactFormComponent),
            multi: true
        },
        {
            provide: NG_VALIDATORS,
            useExisting: forwardRef(() => UserContactFormComponent),
            multi: true
        }
    ]
})
export class UserContactFormComponent implements ControlValueAccessor, Validator {

    contactForm: FormGroup;
    private onValidatorChange: () => void;

    constructor(private formBuilder: FormBuilder) {

        this.contactForm = this.formBuilder.group({
            phone: ['', [Validators.required, Validators.minLength(9)]],
            email: ['', [Validators.required, Validators.email], contactDomainValidator]
        });

    }

    public onTouched: () => void = () => {
    }

    writeValue(value: any): void {
        value && this.contactForm.patchValue(value);

        // interval(100)
        //     .pipe(first(() => this.contactForm.status !== 'PENDING'))
        //     .subscribe(() => this.contactForm.updateValueAndValidity());
    }

    registerOnChange(fn: any): void {
        this.contactForm.valueChanges.subscribe(fn);
    }

    registerOnValidatorChange(fn: () => void): void {
        this.onValidatorChange = fn;

        this.contactForm.statusChanges.subscribe(fn);
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState?(isDisabled: boolean): void {
        isDisabled ? this.contactForm.disable() : this.contactForm.enable();
    }

    validate(c: AbstractControl) {
        return this.contactForm.valid ? null : {contact: {valid: false}};
    }
}

const contactDomainValidator: AsyncValidatorFn = control => {

    if (!control || !control.value) {
        return of(null);
    }

    return timer(1000)
        .pipe(
            map(() => {
                    if (control.value.endsWith('.cz')) {
                        return null;
                    }

                    return {
                        serverMessage: {
                            message: `Only CZ domains are allowed`
                        }
                    };
                }
            )
        );
};
