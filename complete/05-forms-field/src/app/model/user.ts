export interface Contact {
  phone: string;
  email: string;
}

export interface User {
  name: string;
  surname: string;
  twitterName?: string;
  contact: Contact;
  registrationDate?: Date;
}
