import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { ReactiveBasedModule } from './reactive-based/reactive-based.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    SharedModule,
    ReactiveBasedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
