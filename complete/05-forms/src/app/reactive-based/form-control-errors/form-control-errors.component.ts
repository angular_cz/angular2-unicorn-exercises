import { Component, OnInit, Host, Input } from '@angular/core';
import { FormControl, FormGroupDirective, ControlContainer } from '@angular/forms';

@Component({
  selector: 'app-form-control-errors',
  templateUrl: './form-control-errors.component.html'
})
export class FormControlErrorsComponent implements OnInit {

  @Input() controlName: string;

  formControl: FormControl;

  constructor(@Host() private formGroupDirective: FormGroupDirective,
              @Host() private controlContainer: ControlContainer) {
  }

  ngOnInit(): void {
    const controlPath = [...this.controlContainer.path, this.controlName];
    this.formControl = this.formGroupDirective.form.get(controlPath) as FormControl;
  }

  shouldDisplayErrors() {
    return this.formControl.touched || this.formGroupDirective.submitted;
  }

  get errors() {
    return this.formControl.errors;
  }

}
