import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
    {path: 'home', component: HomeComponent},
    {path: 'chat', loadChildren: () => import('src/app/chat/chat.module').then(module => module.ChatModule)},
    {path: 'blind-chat', loadChildren: () => import('src/app/blind-chat/blind-chat.module').then(module => module.BlindChatModule)},
    {path: '', redirectTo: 'home', pathMatch: 'full'}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: []
})
export class AppRoutingModule {
}
