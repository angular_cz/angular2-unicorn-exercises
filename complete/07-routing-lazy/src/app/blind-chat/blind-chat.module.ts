import { NgModule } from '@angular/core';
import { ChatModule } from '../chat/chat.module';
import { RouterModule, Routes } from '@angular/router';
import { RoomsResolver } from '../chat/rooms-resolver.service';
import { ChatComponent } from '../chat/chat.component';
import { RoomsComponent } from '../chat/rooms/rooms.component';
import { ChatDetailComponent } from '../chat/chat-detail/chat-detail.component';
import { BlindChatService } from './blind-chat.service';
import { ChatService } from '../chat/chat.service';


const routes: Routes = [
  {
    path: 'chat', component: ChatComponent,
    resolve: {rooms: RoomsResolver},
    children: [
      {path: '', redirectTo: 'rooms', pathMatch: 'full'},
      {path: 'rooms', component: RoomsComponent, resolve: {rooms: RoomsResolver}},
    ]
  }
];

@NgModule({
  imports: [
    ChatModule,
    RouterModule.forChild(routes)],
  exports: [
    RouterModule,
    ChatComponent
  ],
  providers: [{
    provide: ChatService,
    useClass: BlindChatService,
  }]
})
export class BlindChatModule {
}
