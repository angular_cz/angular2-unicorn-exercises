import { Component, OnInit } from '@angular/core';
import { ChatService } from './chat.service';
import { Observable } from 'rxjs';
import { Room } from './model/chat';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html'
})
export class ChatComponent {

  rooms$: Observable<Room[]>;

  constructor(private route: ActivatedRoute) {
    this.rooms$ = route.data.pipe(map(({rooms}) => rooms));
  }

}
