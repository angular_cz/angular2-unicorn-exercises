import { Component, OnInit, OnDestroy } from '@angular/core';
import { ChatRoom, Message } from '../model/chat';

import { interval, merge, Observable, Subject, Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { startWith, flatMap, tap } from 'rxjs/operators';
import { ChatService } from './chat.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit, OnDestroy {


  chatRoom$ =  this.chatService.chatRoom$;


  constructor(private chatService: ChatService) {
  }

  ngOnInit(): void {

  }

  sendMyMessage(message: Message) {
    this.chatService.postMessage(message);
  }

  ngOnDestroy() {
  }
}
