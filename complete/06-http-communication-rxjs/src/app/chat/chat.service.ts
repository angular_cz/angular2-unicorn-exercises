import { Injectable } from '@angular/core';
import { ChatRoom, Message } from "../model/chat";
import { interval, merge, Observable, Subject } from "rxjs";
import { flatMap, share, shareReplay, startWith, tap } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  readonly CHATROOM_URL = 'http://localhost:8000/rooms/1';

  myMessages$ = new Subject();
  myMessagesStream$: Observable<Response>;

  chatRoom$: Observable<ChatRoom>;

  constructor(private http: HttpClient) {

    this.myMessagesStream$ = this.myMessages$
      .pipe(
        flatMap(message => this.http.post<Response>(this.CHATROOM_URL, message))
      );

    this.chatRoom$ = merge(interval(1000), this.myMessagesStream$)
      .pipe(
        startWith(''),
        flatMap(() => this.http.get<ChatRoom>(this.CHATROOM_URL)),
        tap(() => console.log('chatReloaded')),
        shareReplay(1)
      );
  }

  postMessage(message: Message) {
    this.myMessages$.next(message);
  }


}
