import { Component, Input } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';

@Component({
  selector: 'app-form-metadata-viewer',
  templateUrl: 'form-metadata-viewer.component.html'
})
export class FormMetadataViewerComponent {

  // Temporary solution before union type works
  @Input() control: Partial<NgForm & FormControl>;

}
